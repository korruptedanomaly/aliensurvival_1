﻿using UnityEngine;
using System.Collections;

public class waves : MonoBehaviour {

    public int killedEnemies = 0;
    public int wave= 1;
    //public Transform enemy;
    public Transform[] spawnPoints;
    public GameObject[] enemies;
    public GameObject currentEnemy;
    public bool hasSpawned = false;
    public int spawnTime;
    
	
        
        // Use this for initialization
	void Start () {
        wave = 1;
        InvokeRepeating("Spawn", spawnTime, spawnTime);
	}
	
	// Update is called once per frame
	void Spawn()
    {
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        Instantiate(currentEnemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);

	}

    void Update()
    {
        if (wave == 1)
        {
            currentEnemy = enemies[Random.Range(0, enemies.Length)];
        }
        
        
    }
        
    
}

