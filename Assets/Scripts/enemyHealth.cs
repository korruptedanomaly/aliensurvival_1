﻿using UnityEngine;
using System.Collections;
using System;

public class enemyHealth : MonoBehaviour
{

    public int enemyHP = 5;

    public GameObject explosionEffect;
    public AudioClip explosionSound;

    private AudioSource source;
    private float volLowRange = .5f;
    private float volHighRange = 1.0f;


    void Awake()
    {
        source = GetComponent<AudioSource>();
    }


  

    // Update is called once per frame
    void Update()
    {
        if (enemyHP == 0)
        {
            Debug.Log("Killed");
            Vector3 temp = transform.position;
            temp.y += 10f;
            Instantiate(explosionEffect, temp, Quaternion.identity);
            float vol = UnityEngine.Random.Range(volLowRange, volHighRange);
            source.PlayOneShot(explosionSound, vol);
            Destroy(gameObject);         
        }


    }

   
}