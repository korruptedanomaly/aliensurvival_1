﻿using UnityEngine;
using System.Collections;

public class DestroyFlash : MonoBehaviour {

    private float timeToDestroy = .1f;

	// Use this for initialization
	void Start () {
        Destroy();
	}
	
	void Destroy()
    {
        Destroy(gameObject, timeToDestroy);
    }

}
