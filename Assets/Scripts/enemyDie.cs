﻿using UnityEngine;
using System.Collections;

public class enemyDie : MonoBehaviour
{


    private GameObject enemy;
    private enemyHealth enemyHP;

    void Awake()
    {
        enemy = GameObject.FindWithTag("Enemy");
        //enemyHP = enemy.GetComponent<enemyHealth>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            //Destroy(other.gameObject);
            enemyHP = other.GetComponent<enemyHealth>();
            enemyHP.enemyHP -= 1;

            Destroy(gameObject);
            
        }
        if (other.tag == "ground")
        {
            Destroy(gameObject);
        }
    }
}
