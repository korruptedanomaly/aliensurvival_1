﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class killPlayer : MonoBehaviour {

    public GameObject realPlayer;
    public VRTK.VRTK_TouchpadWalking touchPadWalk;

 

    void Awake()
    {
        realPlayer = GameObject.FindWithTag("realPlayer");
        touchPadWalk = realPlayer.GetComponent<VRTK.VRTK_TouchpadWalking>();
    } 
    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == ("realPlayer"))
        {
            touchPadWalk.maxWalkSpeed = 0;
            print("tagged");
            

            StartCoroutine(Restart());
       }

          }

    IEnumerator Restart()
    {
        yield return new WaitForSeconds(4);
        SceneManager.LoadScene("Scene_1");
    }
}
