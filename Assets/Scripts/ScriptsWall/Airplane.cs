﻿using UnityEngine;
using System.Collections;

public class Airplane : MonoBehaviour {

    public GameObject explosionEffect;
    public AudioClip explosionSound;

    private AudioSource source;
    private float volLowRange = .5f;
    private float volHighRange = 1.0f;


    void Awake ()
    {
        source = GetComponent<AudioSource>();
    }

    void OnTriggerEnter()
    {
        // temp = attach explosion to gameobject
        Vector3 temp = transform.position;
        temp.y += 10f;
        Instantiate(explosionEffect, temp, Quaternion.identity);
        float vol = Random.Range(volLowRange, volHighRange);
        source.PlayOneShot(explosionSound, vol);
        Destroy(this.gameObject);
    }
}
