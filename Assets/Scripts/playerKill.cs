﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class playerKill : MonoBehaviour
{

    public GameObject dieText;


    void Awake()
    {
        //dieText = GameObject.FindWithTag("Canvas");
    }




    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            print("tagged");
            other.GetComponent<CharacterController>().enabled = false;
            dieText.GetComponent<Text>().enabled = true;
            StartCoroutine(Restart());
        }
    }

    IEnumerator Restart()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("FarmLevel02");
    }
}