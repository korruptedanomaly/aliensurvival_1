﻿using UnityEngine;
using System.Collections;

public class MuzzleFire : MonoBehaviour {

    public GameObject muzzleFlash;
    public Transform spawnPoint;

    public SteamVR_TrackedObject rightController;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        var device = SteamVR_Controller.Input((int)rightController.index);
        if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            // Instantiate(muzzleFlash, spawnPoint.transform.position, Quaternion.identity);
            GameObject go = Instantiate(muzzleFlash, spawnPoint.position,   spawnPoint.transform.rotation) as GameObject;
            go.transform.Rotate(90f, 0f, 0f);
        }


    }
}
