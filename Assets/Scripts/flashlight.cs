﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flashlight : MonoBehaviour {

    public SteamVR_TrackedObject leftController;
    public GameObject flashlightTop;
    

    private bool on = true;

    // Use this for initialization
    void Start()
    {



    }

    // Update is called once per frame
    void Update()
    {

        var device = SteamVR_Controller.Input((int)leftController.index);
        if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            on = !on;
        }

        if (on)
        {
            flashlightTop.SetActive(true);
      
        }

        else if (!on)
        {
            flashlightTop.SetActive(false);
          
        }
    }
}